package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;


public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
        
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    

    public void run() {
        // TODO: Implement Rock Paper Scissors

        

        while(true) {
            System.out.println("Let's play round " + roundCounter);
            String humanChoice = readInput("Your choice (Rock/Paper/Scissors)?");
            humanChoice = humanChoice.toLowerCase();
            String computerChoice = generateComputerChoice();

            if(answerIsValid(humanChoice)){
                givePoint(humanChoice,computerChoice);
                System.out.println("Score: human " + humanScore +", computer " + computerScore);
                roundCounter++;
                
                String newRoundChoice = readInput("Do you wish to continue playing? (y/n)?");
                newRoundChoice = newRoundChoice.toLowerCase();
                if(newRoundChoice.equals("n")){
                    System.out.println("Bye bye :)");
                    break;
                }

            }else{
                System.out.println("I do not understand " + humanChoice + ". Could you try again?");
                continue;
            } 
        }
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }
    boolean answerIsValid(String humanInput){
        return humanInput.equals("rock") || humanInput.equals("paper") || humanInput.equals("scissors");
    }
    String generateComputerChoice(){
        double randDouble = Math.floor(Math.random()*3);
        int randInt = (int) randDouble;
        String computerChoice = rpsChoices.get(randInt);
        return computerChoice;
    }
    boolean checkWinner(String input1, String input2){
        if(input1.equals("paper")){
            return input2.equals("rock");
        }
        else if(input1.equals("scissors")){
            return input2.equals("paper");
        }
        else{
            return input2.equals("scissors");
        }

    }
    void givePoint(String humanInput, String computerInput){
        if(checkWinner(humanInput, computerInput)){
            System.out.println("Human chose " + humanInput + ", computer chose " + computerInput +". Human wins!");
            humanScore++;
        }else if(checkWinner(computerInput, humanInput)){
            System.out.println("Human chose " + humanInput + ", computer chose " + computerInput +". Computer wins!");
            computerScore++;
        }else{
            System.out.println("Human chose " + humanInput + ", computer chose " + computerInput +". It's a tie!");
        }

    }
    

}
